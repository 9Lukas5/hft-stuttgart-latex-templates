# LaTeX Templates & classes for HFT Stuttgart

This repository shares LaTeX classes and templates,
for use with documents written in context of University of Applied Sciences Stuttgart.

These are __NOT official__ template by the university,
instead these are developed by the contributors of this repository
and shared __WITHOUT ANY WARRANTY__.

Following chapters will give you a short description and possible requirements for the files to be working.

Disclaimer: I use VSCode with the LaTeX Workshop Plugin and texlive as latex environment.
This will be the environment I tested things to work.

## 9lukas5_hft_article.cls

This file is a LaTeX class file, based onto the article class.

It can be downloaded and placed into the same folder as your documents root file.

This file adds a header with a HFT text left, the document title centered,
and the date right.
The footer shows some additional info left, can show a version and license if set (also left, but below) and page x of y on the right.

The class requires quite a few packages I often need, to pass them options, include them to the documentclass line.

As a minimal example your documentclass call has to look like this:
```tex
\documentclass[german,11pt]{9lukas5_hft_article}
```

Inside the document place labels at the end of front/main matter,
and pageref them on the start of them. Example:

```tex
\begin{document}
    Titlepage here

    \frontmatter{\pageref{endFrontMatter}}
    Some frontmatter things
    \label{endFrontMatter}

    \mainmatter{\pageref{endMainMatter}}
    Some mainmatter stuff
    \label{endMainMatter}
\end{document}
```

To get the thing to compile, you'll need atleast to set a title and author.  
Working minimal example:

```tex
\documentclass[german,11pt]{9lukas5_hft_article}

\title{test}
\author{test}

\begin{document}
    Titlepage here

    \frontmatter{\pageref{endFrontMatter}}
    Some frontmatter things
    \label{endFrontMatter}

    \mainmatter{\pageref{endMainMatter}}
    Some mainmatter stuff
    \label{endMainMatter}
\end{document}

```

### Additional commands defined

|command|prerequisites|description|
|-|-|-|
|`\makePdfMetadata`|packages: hyperref|sets the docuemnts title and author metadata tags|

## titlepage.tex

This is a example for a title page containing the HFT Logo and some Title Information on top, and more metadata stuff on the bottom in a table.

To use with the logo, you'll have to download it from [here][hftlogo], and place it into a folder called `assets` relative from where you put the titlepage.tex file.

To include it into your document, use the package `import` and call
```tex
\subimport{<relative folder path>}{<file>}
```

## main.tex

This is a example of a main file I used recently (recently as in writing this readme^^), to give you an example if you are new to LaTeX.

Feel free to use and strip down/extend to your needs.

## .latexmkrc

config file for latexmk (which I use).
This adds the possibility to place the class file e.g. into another directory, as long as it is in the search path.  

The bottom part make my acronym/glossary things working

[hftlogo]: https://www.hft-stuttgart.de/fileadmin/Dateien/Marketing/Pressemitteilungen/HFT-logo-gross-Aplusheadline.jpg
